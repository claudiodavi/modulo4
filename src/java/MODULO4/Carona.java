/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author claudio
 */
public class Carona extends VerificadorCarona {

    private String campusOrigem;
    private String campusDestino;
    private Date dataHora_Saida;
    private Date dataHora_Chegada;
    private Caronista caronista;
    private String material;
    private Date dataHora_registro;

    public String getCampusOrigem() {
        return campusOrigem;
    }

    public void setCampusOrigem(String campusOrigem) throws ValoresIlegaisException {
        if (verificaCampusExistente(campusOrigem)) {
            this.campusOrigem = campusOrigem;
        } else {
            String erro = "Campus inexistente";
            throw new ValoresIlegaisException(erro);
        }
    }

    public String getCampusDestino() {
        return campusDestino;
    }

    public void setCampusDestino(String campusDestino) throws ValoresIlegaisException {
        if (verificaCampusExistente(campusDestino)) {
            this.campusDestino = campusDestino;
        } else {
            String erro = "Campus inexistente";
            throw new ValoresIlegaisException(erro);
        }
    }

    public Date getDataHora_Saida() {
        return dataHora_Saida;
    }

    public void setDataHora_Saida(String dataHora_Saida) throws ValoresIlegaisException {

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.dataHora_Saida = (Date) formatter.parse(dataHora_Saida);
        } catch (ParseException ex) {
            String erro = "Formato de data inválido";
            throw new ValoresIlegaisException(erro);
        }
    }

    public Date getDataHora_Chegada() {
        return dataHora_Chegada;
    }

    public void setDataHora_Chegada(String dataHora_Chegada) throws ValoresIlegaisException {

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.dataHora_Chegada = (Date) formatter.parse(dataHora_Chegada);
        } catch (ParseException ex) {
            String erro = "Formato de data inválido";
            throw new ValoresIlegaisException(erro);
        }
    }

    public Caronista getCaronista() {
        return caronista;
    }

    public void setCaronista(Caronista caronista) {
        this.caronista = caronista;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {

        this.material = material;
    }

    public Date getDataHora_registro() {

        return dataHora_registro;
    }

    public void setDataHora_registro(String dataHora_registro) throws ValoresIlegaisException {


        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date data = (Date) formatter.parse(dataHora_registro);
            this.dataHora_registro = data;
        } catch (ParseException ex) {
            String erro = "Formato de data inválido";
            throw new ValoresIlegaisException(erro);
        }
    }
}
