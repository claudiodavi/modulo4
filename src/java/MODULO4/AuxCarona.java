/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author claudio
 */
public class AuxCarona {

    public boolean cancelaCarona(Carona ca) throws ValoresIlegaisException {
        DBCarona dbca = new DBCarona();
        Date agora = new Date();
        VerificadorCarona vc = new VerificadorCarona();
        if (vc.verificaAntecedencia(ca.getDataHora_Saida(), agora)) {
            if (ca.getMaterial().isEmpty()) {
                if (dbca.deleteCarona(ca)){
                return true;
            } else return false;
            } else {
                throw new ValoresIlegaisException("Há materiais nessa carona.");
            }
        } else {
            throw new ValoresIlegaisException("Janela de tempo esgotada.");
        }

    }

    public void insereCarona(Carona ca) throws ValoresIlegaisException {
        DBCarona dbca = new DBCarona();
        VerificadorCarona vc = new VerificadorCarona();
        if (ca.getCampusDestino() == null || ca.getCampusOrigem() == null || ca.getCaronista() == null
                || ca.getDataHora_Chegada() == null || ca.getDataHora_Saida() == null || ca.getDataHora_registro() == null) {
            throw new ValoresIlegaisException("Valores nulos inseridos. Carona não registrada");
        } else if (!vc.verificaCampusIguais(ca.getCampusOrigem(), ca.getCampusDestino())
                || !vc.verificaAntecedencia(ca.getDataHora_registro(), ca.getDataHora_Saida())) {
            dbca.insertCarona(ca);
        }
    }

    public void modificaCarona(Carona ca) throws ValoresIlegaisException {
        DBCarona dbca = new DBCarona();

        if (ca.getCampusDestino() == null || ca.getCampusOrigem() == null || ca.getCaronista() == null
                || ca.getDataHora_Chegada() == null || ca.getDataHora_Saida() == null || ca.getDataHora_registro() == null
                || ca.getMaterial() == null) {
            throw new ValoresIlegaisException("Valores nulos inseridos. Carona não modificada");
        } else {
            dbca.modifyCarona(ca);
        }
    }

    public ArrayList retornaCaronaPCaronista(String cpf) throws ValoresIlegaisException {
       
        if(cpf.isEmpty()){
            String erro = "Campo cpf nulo";
            throw new ValoresIlegaisException(erro);
        }
        
        AuxCaronista axct = new AuxCaronista();
        DBCarona dbca = new DBCarona();

        return dbca.returnCarona(axct.retornaCaronista(cpf));

    }
    
    public ArrayList retornaCaronaPData(String saida) throws ValoresIlegaisException {
        if(saida.isEmpty()){
            String erro = "Campo data de saida nulo";
            throw new ValoresIlegaisException(erro);
        }
        AuxCaronista axct = new AuxCaronista();
        DBCarona dbca = new DBCarona();
        
        return dbca.retornaCaronaPData(saida);
        
    }

    public ArrayList listaCarona() throws ValoresIlegaisException {

        DBCarona dbca = new DBCarona();

        return dbca.retornaTodasCaronas();
    }
}
