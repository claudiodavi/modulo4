/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author claudio
 */
public class DBCaronista {

    ConnectionFactory confac = new ConnectionFactory();
    private PreparedStatement AdicionaPessoa;
    private PreparedStatement RetornaCaronista;
    private PreparedStatement Deleta;

    public boolean insertCaronista(Caronista c) {


        try {
            Connection con = confac.criaConexao();

            AdicionaPessoa = con.prepareStatement("INSERT INTO caronista "
                    + "( nomecompleto , nomeusuario , senha , email , cpf , matricula , "
                    + " campusorigem , datanascimento ) " + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ? );");

            AdicionaPessoa.setString(1, c.getNomeCompleto());
            AdicionaPessoa.setString(2, c.getNomeUsuario());
            AdicionaPessoa.setString(3, c.getSenha());
            AdicionaPessoa.setString(4, c.getEmail());
            AdicionaPessoa.setString(5, c.getCpf());
            AdicionaPessoa.setString(6, c.getMatricula());
            AdicionaPessoa.setString(7, c.getCampusOrigem());
            AdicionaPessoa.setDate(8, converteData(c.getDataNascimento()));

            AdicionaPessoa.execute();
            AdicionaPessoa.close();
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean modifyCaronista(Caronista c) {


        Connection con = confac.criaConexao();
        try {

            PreparedStatement ps = con.prepareStatement("UPDATE caronista SET nomecompleto = "
                    + "'" + c.getNomeCompleto() + "'" + ", nomeusuario = " + "'" + c.getNomeUsuario() + "'"
                    + ", senha = " + "'" + c.getSenha() + "'" + ", email = " + "'" + c.getEmail() + "'"
                    + ", matricula = " + "'" + c.getMatricula() + "'" + ", campusorigem = "
                    + "'" + c.getCampusOrigem() + "'" + ", datanascimento = " + converteData(c.getDataNascimento())
                    + " WHERE cpf = " + "'" + c.getCpf() + "'");
            ps.executeQuery();

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }

    public boolean deleteCaronista(Caronista c) {
        Connection con = confac.criaConexao();
        try {
            Deleta = con.prepareStatement("DELETE FROM caronista " + "WHERE cpf = " + c.getCpf());
            if (Deleta.executeUpdate() == 0) {
                System.out.println("Não deletou");
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBCaronista.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception we) {
            we.getCause();
            return false;
        }
        return true;
    }

    public Caronista returnCaronista(String cpf) throws ValoresIlegaisException {
        try {
            Caronista c = null;
            Connection con = confac.criaConexao();

            String SQL = "SELECT * FROM caronista WHERE cpf = " + "'" + cpf + "'";

            RetornaCaronista = con.prepareStatement(SQL);

            ResultSet rs = RetornaCaronista.executeQuery();

            if (rs.next()) {
                c = new Caronista();
                c.setNomeCompleto(rs.getString("nomecompleto"));
                c.zetNomeUsuario(rs.getString("nomeusuario"));
                c.setCampusOrigem(rs.getString("campusorigem"));
                c.setDataNascimento(rs.getString("datanascimento"));
                c.setEmail(rs.getString("email"));
                c.setMatricula(rs.getString("matricula"));
                c.setSenha(rs.getString("senha"));
                c.zetCpf(cpf);
            }
            con.close();

            return c;


        } catch (SQLException ex) {
            Logger.getLogger(DBCaronista.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public ArrayList buscaCaronistaCampus(String campus) throws ValoresIlegaisException, SQLException {
        ArrayList<Caronista> listCaron = null;

        //cria conexão
        Connection con = confac.criaConexao();
        //SQL que chama os cpf's dos caronistas de acordo com o campus
        String SQL = "SELECT cpf FROM caronista WHERE campus = " + "'" + campus + "'";
        java.sql.Statement st;

        try {
            st = con.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            //equanto tiver resultados no result set...
            while (rs.next()) {
                //adiciona na lista o caronista buscado pelo cpf do resultSet
                listCaron.add(returnCaronista(rs.getString("cpf")));

            }
        } catch (SQLException sq) {
            con.close();
            Logger.getLogger(DBCaronista.class.getName()).log(Level.SEVERE, null, sq);
        }
        con.close();
        return listCaron;
    }

    private java.sql.Date converteData(java.util.Date data) {
        java.sql.Date datasql = new java.sql.Date(data.getTime());
        return datasql;
    }

    public void deletaTuto() {
        Connection con = confac.criaConexao();

        String SQL = "DELETE FROM caronista;" + "  DELETE FROM carona;";
        
        try {
            Deleta = con.prepareStatement(SQL);
            System.out.println(Deleta.executeUpdate());
            
             
        } catch (SQLException sq) {
        }
    }
}
