/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author claudio
 */
public class Caronista extends VerificadorCaronista {

    private String nomeCompleto;
    private String nomeUsuario;
    private String senha;
    private String email;
    private String cpf;
    private String matricula;
    private String campusOrigem;
    private Date dataNascimento;

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) throws ValoresIlegaisException {
        if (nomeCompleto.isEmpty()) {
            String erro = "O Nome não pode ser nulo";
            throw new ValoresIlegaisException(erro);
        } else {
            this.nomeCompleto = nomeCompleto;
        }
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) throws ValoresIlegaisException {
        
        if (verificaNomeUsuario(nomeUsuario)) {
            this.nomeUsuario = nomeUsuario;
        } else {
            String erro = "Usuário já existente";
            throw new ValoresIlegaisException(erro);
        }
    }

    @Deprecated
    public void zetNomeUsuario(String nomeUsuario) throws ValoresIlegaisException {
        
        this.nomeUsuario = nomeUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) throws ValoresIlegaisException {
        if (verificaSenhaForte(senha)) {
            this.senha = senha;
        } else {
            String erro = "Senha não condiz com os parâmetros de força definidos.";
            throw new ValoresIlegaisException(erro);

        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) throws ValoresIlegaisException {
        if (verificaEmailValido(email)) {
            this.email = email;
        } else {
            String erro = "Formato de email invalido.";
            throw new ValoresIlegaisException(erro);
        }
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) throws ValoresIlegaisException {
        if (verificaCPFExistente(cpf) && verificaCPFvalido(cpf)) {
            this.cpf = cpf;
        } else {
            String erro = "CPF inválido ou já registrado";
            throw new ValoresIlegaisException(erro);
        }
    }

    @Deprecated
    public void zetCpf(String cpf) throws ValoresIlegaisException {
       
            this.cpf = cpf;
        
    }
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) throws ValoresIlegaisException {

        if (verificaMatricula(matricula)) {
            this.matricula = matricula;
        } else {
            String erro = "Matricula inválida.";
            throw new ValoresIlegaisException(erro);
        }
    }

    public String getCampusOrigem() {
        return campusOrigem;
    }

    public void setCampusOrigem(String campusOrigem) throws ValoresIlegaisException {
        if (verificaCampusOrigem(campusOrigem)) {
            this.campusOrigem = campusOrigem;
        } else {
            String erro = "Capus inserido inválido";
            throw new ValoresIlegaisException(erro);
        }
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNas) throws ValoresIlegaisException {

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date data = (Date) formatter.parse(dataNas);
            if (verificaDataNascimento(data)) {
                this.dataNascimento = data;
            } else {
                String erro = "Sistema permitido apenas para maiores de 18 anos";
                throw new ValoresIlegaisException(erro);
            }
        } catch (ParseException ex) {
            System.out.println("Data inválida");
        }
    }

    public boolean solicitaCarona(Carona c) {
        return false;
    }

    public ArrayList buscaCarona(Calendar data) {
        ArrayList listaCarona = null;

        return listaCarona;
    }
}
