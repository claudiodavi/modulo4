/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author claudio
 */
public class AuxCaronista {

    public void createCaronista(Caronista c) throws ValoresIlegaisException {

        DBCaronista car = new DBCaronista();

        if (c.getCpf() == null || c.getCampusOrigem() == null
                || c.getDataNascimento() == null
                || c.getEmail() == null || c.getMatricula() == null
                || c.getNomeCompleto() == null || c.getNomeUsuario() == null
                || c.getSenha() == null) {
            String erro = "Valores nulos.";
            throw new ValoresIlegaisException(erro);
        } else {
            car.insertCaronista(c);
        }
    }

    public void editCaronista(Caronista c) throws ValoresIlegaisException {
        DBCaronista car = new DBCaronista();

        if (c.getCpf() == null || c.getCampusOrigem() == null
                || c.getDataNascimento() == null
                || c.getEmail() == null || c.getMatricula() == null
                || c.getNomeCompleto() == null || c.getNomeUsuario() == null
                || c.getSenha() == null) {
            String erro = "Valores nulos.";
            throw new ValoresIlegaisException(erro);
        } else {
            car.modifyCaronista(c);
        }

    }

    public Caronista retornaCaronista(String cpf) throws ValoresIlegaisException {
        DBCaronista car = new DBCaronista();
        if (cpf != null) {
            return car.returnCaronista(cpf);
        } else {
            return null;
        }

    }

    public ArrayList buscaCaronistaCampus(String campus) throws ValoresIlegaisException, SQLException {
        if(campus.isEmpty()) throw new ValoresIlegaisException("Valor Nulo");
        
        DBCaronista dbcar = new DBCaronista();

        ArrayList<Caronista> listaCaronista = dbcar.buscaCaronistaCampus(campus);

        return listaCaronista;
    }
}
