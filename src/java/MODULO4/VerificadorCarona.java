/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author claudio
 */
public class VerificadorCarona {

    Connection con;

    protected void conectar() {

        con = ConnectionFactory.criaConexao();
        System.out.println("Conectado!");

    }

    /**
     * 
     * @param Recebe a entrada do registro de carona.
     * @param Recebe também a hora de saída.
     * @return true caso a antecedencia seja confirmada.
     */
    protected boolean verificaAntecedencia(Date regis, Date saida) {

        long registro = regis.getTime();
        long partida = saida.getTime();
        long diferenca = (partida - registro);

        long segundos = diferenca / 1000;
        long minutos = segundos / 60;
        long horas = minutos / 60;

        if (horas >= 24) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se há alguém presente registrado na carona.
     * @param Recebe uma carona
     * @return boolean true caso haja alguém presente, falso caso contrário.
     */
    protected boolean verificaPresenca(Carona ca) {
        if (ca.getCaronista() != null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Verifica se há algum material presente no registro da carona
     * @param Recebe uma Carona
     * @return boolean true caso haja material, false caso contrário
     */
    protected boolean verificaMaterial(Carona ca) {
        if (ca.getMaterial() != null) {
            return false;
        }

        return true;
    }
    
    /**
     * Recebe um campus e verifica se ele existe e é válido de acordo com os
     * registros do Banco de Dados.
     * @param campus
     * @return true caso ele exista e false caso não.
     */

    protected boolean verificaCampusExistente(String campus) {
        if (campus.isEmpty()) {
            return false;
        }
        conectar();
        try {
            java.sql.Statement stm = con.createStatement();
            String SQL = "SELECT campus FROM campus WHERE campus = " + "'" + campus + "'";

            ResultSet rs = stm.executeQuery(SQL);

            if (!rs.next()) {
                System.out.print("Não há campus registrado");
                rs.close();
                con.close();
                return false;
            }
            con.close();

            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    protected boolean verificaCampusIguais(String CampOrig, String CampDest) {
        if (CampDest.equals(CampOrig)) {
            return false;
        }
        return true;
    }

    /**não será implementada a permissão de administrador nessa etapa
     *porém o método consta.
     */
    protected boolean verificaAutorizacao() {
        return false;
    }
}
