/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author claudio
 */
public class VerificadorCaronista {

    Connection con;

    private void conectar() {

        con = ConnectionFactory.criaConexao();
    }

    /**
     * Utiliza uma classe auxiliar com os métodos de verificação por vetores.
     * @param Recebe um caronista
     * @return retorna true caso a senha atenda aos requisitos, e falso caso contrario
     */
    protected boolean verificaSenhaForte(String senha) {
        // Criterio 1: Senha com 6 caracteres ou mais  
        if (senha.length() < 6) {
            System.out.println("Senha contem menos de 6 caracteres");
            return false;
        }

        // Criterio 2: Senha com mais de 10 caracteres
        if (senha.length() > 10) {
            System.out.println("Senha contém mais de 10 caracteres");
            return false;
        }
        // Criterio 3: Senha contém letras maiúsculas  
        Pattern p2 = Pattern.compile("[A-Z]");
        if (!p2.matcher(senha).find()) {
            System.out.println("Senha não contem caracteres maiusculos");
            return false;
        }
        // Criterio 4: Senha contém letras minúsculas  
        Pattern p3 = Pattern.compile("[a-z]");
        if (!p3.matcher(senha).find()) {
            System.out.println("Senha não contem carcteres minusculos");
            return false;
        }
        // Criterio 5: Senha contém dígitos  
        Pattern p4 = Pattern.compile("[0-9]");
        if (!p4.matcher(senha).find()) {
            System.out.println("Senha não contem numeros");
            return false;
        }
        // Critério 6: Senha contém um dos seguintes caracteres especiais: + - * / = %  
        Pattern p5 = Pattern.compile("[-+*/=%!@#$&]");
        if (!p5.matcher(senha).find()) {
            System.out.println("Senha não contém caracteres especiais");
            return false;
        }
        // senão...  
        return true;
    }

    /**
     * 
     * @param recebe o nome de usuario do caronista
     * @return retorna true caso estiver disponivel
     */
    protected boolean verificaNomeUsuario(String nomeUs) {
        if (nomeUs == null) {
            return false;
        }

        conectar();
        try {

            java.sql.Statement st = con.createStatement();
            String SQL = "SELECT nomeusuario FROM caronista WHERE nomeusuario = " + "'" + nomeUs + "'";
            ResultSet rs = st.executeQuery(SQL);
            return !rs.next();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Usuário inválido");
            return false;
        }


    }

    /**
     * O Email deve conter uma @ e pelo menos 1 '.'
     * @param Recebe o email do caronista.
     * @return boolean true caso o teste seja executado com sucesso, false caso contrário
     */
    protected boolean verificaEmailValido(String email) {

        if (email.isEmpty()) {
            System.out.println("Insira um Email");
            return false;
        }
        if (email.contains("@@")) {
            System.out.println("email inválido");
            return false;
        }

        for (int a = 0; a < email.length(); a++) {
            int c = a + 1;
            if (email.charAt(a) == '@' && email.charAt(c) != '@') {
                for (int b = a; b < email.length(); b++) {
                    if (email.charAt(b) == '.') {
                        return true;
                    }
                }
            }
        }
        System.out.println("email inválido");
        return false;
    }

    /**
     * Método baseado na fórmula oferecida pela Receita Federal e também num artigo
     * da DevMedia.
     *
     * @param Recebe o cpf do caronista para fazer a verificação
     * @return boolean true se o cpf for válido. caso contrário não valida.
     */
    protected boolean verificaCPFvalido(String CPF) {
        if (CPF.equals("00000000000") || CPF.equals("11111111111")
                || CPF.equals("22222222222") || CPF.equals("33333333333")
                || CPF.equals("44444444444") || CPF.equals("55555555555")
                || CPF.equals("66666666666") || CPF.equals("77777777777")
                || CPF.equals("88888888888") || CPF.equals("99999999999")
                || (CPF.length() != 11)) {
            System.out.println("cpf inválido");
            return false;
        }
        char dig10, dig11;
        int sm, i, r, num, peso;
        sm = 0;
        peso = 10;

        for (i = 0; i < 9; i++) {
            num = (int) (CPF.charAt(i) - 48);
            sm = sm + (num * peso);
            peso = peso - 1;
        }
        r = 11 - (sm % 11);

        if ((r == 10) || (r == 11)) {
            dig10 = '0';
        } else {
            dig10 = (char) (r + 48);
        }

        sm = 0;
        peso = 11;

        for (i = 0; i < 10; i++) {
            num = (int) (CPF.charAt(i) - 48);
            sm = sm + (num * peso);
            peso = peso - 1;
        }

        r = 11 - (sm % 11);

        if ((r == 10) || (r == 11)) {
            dig11 = '0';
        } else {
            dig11 = (char) (r + 48);
        }

        if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10))) {
            return true;
        } else {
            System.out.println("CPF inválido");
            return false;
        }


    }

    /**
     * Verifica se o CPF já está incluidoem uso no banco de dados.
     *
     * @param recebe o cpf do caronista
     * @return true, caso o cpf exista no banco, false caso ele esteja disponível.
     */
    protected boolean verificaCPFExistente(String cpf) {
        if (cpf == null) {
            return false;
        }
        conectar();

        java.sql.Statement stm = null;
        String SQL = null;
        try {
            stm = con.createStatement();
            SQL = "SELECT cpf FROM caronista WHERE cpf = '" + cpf + "'";

            ResultSet rs;

            rs = stm.executeQuery(SQL);

            return !rs.next();

        } catch (SQLException ex) {
            return false;
        }

    }

    /**
     * 
     * @param Recebe uma String com a matricula do caronista.
     * @return retorna true, caso só haja numeros.
     */
    protected boolean verificaMatricula(String mat) {
        if (mat.isEmpty()) {
            return false;
        }

        if (mat.matches("^[0-9]*$")) {
            return true;
        } else {
            System.out.println("Matricula inválida");
            return false;
        }
    }

    /**
     * 
     * @param recebe o campus de origem do caronista
     * @return retorna true caso exista, e falso caso não esteja registrado no BD
     */
    protected boolean verificaCampusOrigem(String campus) {
        if (campus.isEmpty()) {
            return false;
        }

        conectar();
        try {
            java.sql.Statement stm = con.createStatement();
            String SQL = "SELECT campus FROM campus WHERE campus = " + "'" + campus + "'";

            ResultSet rs = stm.executeQuery(SQL);

            if (!rs.next()) {
                System.out.print("Não há campus registrado");
                rs.close();
                con.close();
                return false;
            }
            con.close();

            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    /**
     * Método que verifica se o Caronista tem mais de 18 anos partindo da data atual
     * @param Recebe a data de nascimento do Caronista.
     * @return retorna verdadeiro se a data atual - data de nascimento 
     * for maior ou igual a 18
     */
    protected boolean verificaDataNascimento(Date nascimento) {
        if (nascimento == null) {
            return false;
        }
        Date dtAtual = new Date();

        long atual = dtAtual.getTime();
        long nasc = nascimento.getTime();
        long idade = (atual - nasc);
        long segundos = idade / 1000;
        long minutos = segundos / 60;
        long horas = minutos / 60;
        long dias = horas / 24;
        long anos = dias / 365;

        if (anos >= 18) {
            return true;
        }
        System.out.println("Usuário menor de 18 anos");
        return false;
    }
}
