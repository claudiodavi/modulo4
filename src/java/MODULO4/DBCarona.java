/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author claudio
 */
public class DBCarona {

    ConnectionFactory confac = new ConnectionFactory();

    public boolean insertCarona(Carona ca) {



        ConnectionFactory confac = new ConnectionFactory();
        try {
            Connection con = confac.criaConexao();
            PreparedStatement adicionaCarona = con.prepareStatement("INSERT INTO carona"
                    + "(campusOrigem, campusdestino, saida, chegada, cpf_caronista, material, registro)"
                    + "VALUES (?, ?, ?, ?, ?, ?, ?);");


            adicionaCarona.setString(1, ca.getCampusOrigem());
            adicionaCarona.setString(2, ca.getCampusDestino());
            adicionaCarona.setDate(3, converteData(ca.getDataHora_Saida()));
            adicionaCarona.setDate(4, converteData(ca.getDataHora_Chegada()));
            adicionaCarona.setString(5, ca.getCaronista().getCpf());
            adicionaCarona.setString(6, ca.getMaterial());
            adicionaCarona.setDate(7, converteData(ca.getDataHora_registro()));

            adicionaCarona.executeUpdate();
            adicionaCarona.close();
            con.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }


    }

    public boolean deleteCarona(Carona ca) {

        ConnectionFactory confac = new ConnectionFactory();
        try {
            Connection con = confac.criaConexao();
            PreparedStatement deletaCarona = con.prepareStatement("DELETE FROM carona"
                    + " WHERE cpf_caronista = '" + ca.getCaronista().getCpf() + "';");
            int i = deletaCarona.executeUpdate();
            deletaCarona.close();
            con.close();
            if (i == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }

    public boolean modifyCarona(Carona ca) {
        ConnectionFactory confac = new ConnectionFactory();
        try {
            Connection con = confac.criaConexao();

            PreparedStatement ps = con.prepareStatement("UPDATE carona SET campusdestino = " + "'" + ca.getCampusDestino() + "'"
                    + ", campusorigem = '" + ca.getCampusOrigem() + "'" + ", saida = '" + ca.getDataHora_Saida() + "'" + ", chegada =  '"
                    + ca.getDataHora_Chegada() + "'" + ", material = '" + ca.getMaterial() + "'" + ", registro = '"
                    + ca.getDataHora_registro() + "'" + " WHERE cpf_caronista = '" + ca.getCaronista().getCpf() + "';");

            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ArrayList returnCarona(Caronista ct) {
        ArrayList listacarona = new ArrayList();

        ConnectionFactory confac = new ConnectionFactory();

        try {
            Connection con = confac.criaConexao();
            String SQL = "SELECT * FROM carona WHERE caronista_cpf = '" + ct.getCpf() + "';";
            java.sql.Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            while (rs.next()) {
                Carona c = new Carona();
                c.setCampusDestino(rs.getString("campusdestino"));
                c.setCampusOrigem(rs.getString("campusorigem"));
                c.setDataHora_Chegada(rs.getString("chegada"));
                c.setDataHora_Saida(rs.getString("saida"));
                c.setDataHora_registro(rs.getString("registro"));
                c.setMaterial(rs.getString("material"));
                c.setCaronista(ct);
                listacarona.add(c);

            }


            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listacarona;
    }

    public ArrayList retornaTodasCaronas() throws ValoresIlegaisException {
        ConnectionFactory confac = new ConnectionFactory();
        AuxCaronista axct = new AuxCaronista();
        ArrayList<Carona> listaCaronas = null;
        try {
            Connection con = confac.criaConexao();
            String SQL = "SELECT * FROM carona;";
            java.sql.Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(SQL);

            while (rs.next()) {
                Carona c = new Carona();
                c.setCampusDestino(rs.getString("campusdestino"));
                c.setCampusOrigem(rs.getString("campusorigem"));
                c.setDataHora_Chegada(rs.getString("chegada"));
                c.setDataHora_Saida(rs.getString("saida"));
                c.setDataHora_registro(rs.getString("registro"));
                c.setMaterial(rs.getString("material"));
                c.setCaronista(axct.retornaCaronista(rs.getString("cpf_caronista")));
                listaCaronas.add(c);
            }
            return listaCaronas;

        } catch (Exception e) {
            throw new ValoresIlegaisException();
        }
    }
    
    public ArrayList retornaCaronaPData(String saida) throws ValoresIlegaisException{
        ConnectionFactory confac = new ConnectionFactory();
        AuxCaronista axct = new AuxCaronista();
        ArrayList<Carona> listaCaronas = null;
        try {
            Connection con = confac.criaConexao();
            String SQL = "SELECT * FROM carona WHERE saida = ' "+saida+"';";
            java.sql.Statement st;
           
                st = con.createStatement();            
            ResultSet rs = st.executeQuery(SQL);
          
                while (rs.next()) {
                    Carona c = new Carona();
                    c.setCampusDestino(rs.getString("campusdestino"));
                    c.setCampusOrigem(rs.getString("campusorigem"));
                    c.setDataHora_Chegada(rs.getString("chegada"));
                    c.setDataHora_Saida(rs.getString("saida"));
                    c.setDataHora_registro(rs.getString("registro"));
                    c.setMaterial(rs.getString("material"));
                    c.setCaronista(axct.retornaCaronista(rs.getString("cpf_caronista")));
                    listaCaronas.add(c);
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBCarona.class.getName()).log(Level.SEVERE, null, ex);
            }
            return listaCaronas;
        
    }

    private java.sql.Date converteData(java.util.Date data) {
        java.sql.Date datasql = new java.sql.Date(data.getTime());
        return datasql;
    }
}
