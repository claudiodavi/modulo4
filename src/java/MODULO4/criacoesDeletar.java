/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

/**
 *
 * @author claudio
 */
public class criacoesDeletar {

    public Caronista criaCaronistaPerfeito() throws ValoresIlegaisException{
        Caronista ct = new Caronista();
        ct.setCampusOrigem("Alegrete");
        ct.setCpf("87375338515");
        ct.setDataNascimento("02/09/1993");
        ct.setEmail("cdavisouza@gmail.com");
        ct.setMatricula("111150193");
        ct.setNomeCompleto("Claudio Davi");
        ct.setNomeUsuario("claudiodavi");
        ct.setSenha("testesenha123@");

        return ct;
    }

    public Caronista CaronistaCampusInexistente() throws ValoresIlegaisException{
        Caronista ct2 = new Caronista();
        ct2.setCampusOrigem("Porto Alegre");
        ct2.setCpf("79653057120");
        ct2.setDataNascimento("05/05/1991");
        ct2.setEmail("exemplo@exemplo.com");
        ct2.setMatricula("11253423");
        ct2.setNomeCompleto("Exemplino Silva");
        ct2.setNomeUsuario("exemplino");
        ct2.setSenha("TesteSenha@@");

        return ct2;
    }

    public Carona criaCarona1(Caronista ct) {
        Carona ca = new Carona();

        ca.setCampusDestino("Livramento");
        ca.setCampusOrigem("Alegrete");
        ca.setCaronista(ct);
        ca.setDataHora_Chegada("28/01/2013");
        ca.setDataHora_Saida("28/01/2013");
        ca.setDataHora_registro("26/01/2013");
        ca.setMaterial(null);

        return ca;
    }

    public Carona criaCarona2(Caronista ct) {
        Carona ca2 = new Carona();

        ca2.setCampusDestino("Porto Alegre");
        ca2.setCampusOrigem("Livramento");
        ca2.setCaronista(ct);
        ca2.setDataHora_Chegada("28/01/2013");
        ca2.setDataHora_Saida("28/01/2013");
        ca2.setDataHora_registro("28/01/2013");
        ca2.setMaterial("Computador Sony Vaio");

        return ca2;
    }
}
