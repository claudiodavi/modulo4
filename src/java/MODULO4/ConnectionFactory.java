/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author claudio
 */
public class ConnectionFactory {

    static String driver = "org.postgresql.Driver";
    static String user = "postgres";
    static String pass = "estudar";
    static String url = "jdbc:postgresql://localhost:5432/RP4final";

    public static Connection criaConexao() {
       Connection con = null;
     
       
       try {
            Class.forName(driver);
            

            con = (Connection) DriverManager.getConnection(url, user, pass);

            
            System.out.println("Conexão realizada com sucesso \n");

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException sq) {
            sq.printStackTrace();
        }
        
        return  con;
    }
}