create sequence seq_motorista;
create sequence seq_cnh;
create sequence seq_campus;
create sequence seq_viagem;

CREATE TABLE IF NOT EXISTS campus(
idcampus integer default nextval('seq_campus') not null,
campus varchar(30) not null,
primary key(campus));

insert into campus(campus) values('alegrete');
insert into campus(campus) values('uruguaiana');
insert into campus(campus) values('sao borja');
insert into campus(campus) values('livramento');
insert into campus(campus) values('itaqui');
insert into campus(campus) values('jaguarao');
insert into campus(campus) values('dom pedrito');
insert into campus(campus) values('bage');
insert into campus(campus) values('caçapava');
insert into campus(campus) values('sao gabriel');

CREATE TABLE IF NOT EXISTS motorista(
idmotorista integer default nextval('seq_motorista') not null,
nome varchar(50) not null,
login varchar(30) not null,
senha varchar(30) not null,
email varchar(30) not null,
cpf varchar(11) not null,
campus varchar(30) not null,
dataNascimento date not null,
dataContratacao date not null,
status boolean,
primary key (cpf),
unique (login),
foreign key (campus) references campus);


CREATE TABLE IF NOT EXISTS cnh(
idcnh integer default nextval('seq_cnh') not null,
numeroCNH varchar(11) not null,
categoria varchar(5) not null,
cpf varchar(11) not null,
primary key(numeroCNH),
foreign key (cpf) references motorista);

CREATE TABLE IF NOT EXISTS caronista
(
  nomecompleto character varying(30) NOT NULL,
  nomeusuario character varying(20) NOT NULL,
  senha character varying(15) NOT NULL,
  email character varying(30) NOT NULL,
  cpf character varying(11) NOT NULL,
  matricula character varying(12) NOT NULL,
  campusorigem character varying(10) NOT NULL,
  datanascimento character varying(40) NOT NULL,
  CONSTRAINT caronista_pkey PRIMARY KEY (cpf ),
  CONSTRAINT caronista_campusorigem_fkey FOREIGN KEY (campusorigem)
      REFERENCES campus (campus)
	);
CREATE TABLE IF NOT EXISTS carona ( 
  campusorigem character varying(10) NOT NULL,
  campusdestino character varying(10) NOT NULL,
  saida character varying(20) NOT NULL,
  chegada character varying(20) NOT NULL,
  cpf_caronista character varying(12) NOT NULL,
  material character varying(10),
  registro character varying(20) NOT NULL,
 CONSTRAINT carona_campusdestino_fkey FOREIGN KEY (campusdestino)
      REFERENCES campus (campus),
 CONSTRAINT carona_campusorigem_fkey FOREIGN KEY (campusorigem)
      REFERENCES campus (campus),
 CONSTRAINT carona_cpf_caronista_fkey FOREIGN KEY (cpf_caronista)
      REFERENCES caronista (cpf) 
	);

	
CREATE TABLE IF NOT EXISTS viagem (
  codviagem integer default nextval ('seq_viagem') not null,
  data date NOT NULL,
  hora date NOT NULL,
  material varchar(45) NOT NULL,
  c_origem varchar(45) NOT NULL,
  c_destino varchar(45) NOT NULL,
  veiculo_codVeiculo integer NOT NULL,
  motorista_codMot integer NOT NULL,
	PRIMARY KEY (codviagem),
	UNIQUE (veiculo_codVeiculo, motorista_codMot)
); 
	
