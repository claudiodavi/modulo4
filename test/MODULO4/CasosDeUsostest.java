/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.util.ArrayList;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author claudio
 */
public class CasosDeUsostest {

    public CasosDeUsostest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        DBCaronista dbca = new DBCaronista();
        dbca.deletaTuto();
        //Ao iniciar os testes, faz uma limpa no banco de dados

    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Cria um caronista com todos os valores válidos, insere no Banco de Dados
     * Depois, busca o caronista inserido pelo cpf.
     * Se retornar os mesmos dados do objeto anterior o teste foi realizado com sucesso
     * 
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test
    public void testCreateCaronistaPerfeito() throws Exception {
        System.out.println("createCaronista");
        Caronista c = new Caronista();
        //Atribui todos os valores corretos do Caronista
        c.setCampusOrigem("uruguaiana");
        c.setCpf("83953666630");
        c.setDataNascimento("12/10/1990");
        c.setEmail("carlos@exemplo.com");
        c.setMatricula("89281");
        c.setNomeCompleto("Carlos Eduardo");
        c.setNomeUsuario("carloseduardo");
        c.setSenha("Senha@123");

        //manda criar o caronista
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
        //chama o método que retorna
        Caronista compara = instance.retornaCaronista("83953666630");
        //compara os dados pra ver se estão iguais.
        assertTrue(c.getEmail().equals(compara.getEmail()) && c.getNomeCompleto().equals(compara.getNomeCompleto()));

    }

    /**
     * Cria um caronista que possui o mesmo cpf que o criado anteriormente
     * deve retornar a excessão de que o cpf já é existente.
     * 
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCreateCaronistaMesmoCPF() throws Exception {
        System.out.println("createCaronista");
        Caronista c = new Caronista();
        //atribui os dados do caronista, porém com cpf igual ao anterior
        c.setCampusOrigem("uruguaiana");
        c.setCpf("83953666630");
        c.setDataNascimento("12/10/1990");
        c.setEmail("regina@exemplo.com");
        c.setMatricula("89281");
        c.setNomeCompleto("Regina Lacerda");
        c.setNomeUsuario("reginalacerda");
        c.setSenha("Senha@123");
        //chama o mpetodo que cria e insere no Banco de Dados
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);


    }

    /**
     * Teste que recebe a entrada de um cpf nulo, e outros todos valores válidos.
     * Deve lançar a excessão de valores ilegais tratada no metodo Set do Caronista.
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCpfNulo() throws ValoresIlegaisException {
        System.out.println("CPF NULO");

        Caronista c = new Caronista();
        //atribui os dados certinho
        c.setCampusOrigem("uruguaiana");
        c.setCpf(""); // atribui um cpf nulo
        c.setDataNascimento("12/10/1990");
        c.setEmail("regina@exemplo.com");
        c.setMatricula("89281");
        c.setNomeCompleto("Regina Lacerda");
        c.setNomeUsuario("reginalacerda");
        c.setSenha("Senha@123");
        //tenta fazer a inserção com cpf nulo
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os valores válidos e um cpf com números aleatórios
     * deve retornar a excessão de cpf inválido
     * @throws ValoresIlegaisException
     * 
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCpfInvalido() throws ValoresIlegaisException {
        System.out.println("CPF inválido");

        Caronista c = new Caronista();
        //atribui os dados certinho
        c.setCampusOrigem("uruguaiana");
        c.setCpf("12345678910"); // atribui um cpf com numeros aleatórios
        c.setDataNascimento("12/10/1990");
        c.setEmail("jorge@exemplo.com");
        c.setMatricula("89281");
        c.setNomeCompleto("Jorge Lacerda");
        c.setNomeUsuario("jorgelacerda");
        c.setSenha("Senha@123");
        //tenta inserir normalmente
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Testa o CPF, todos os números foram inseridos corretamente porém o dígito
     * verificador é aleatório, portanto deve retornar a excessão
     * @throws ValoresIlegaisException 
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCpfdigitoInvalido() throws ValoresIlegaisException {
        System.out.println("CPF Digito verificador inválido");

        Caronista c = new Caronista();
        //atribui todos os dados certo
        c.setCampusOrigem("alegrete");
        c.setCpf("17202047121");//os primeiros dígitos estão corretos, porém o verificador é aleatório
        c.setDataNascimento("12/10/1990");
        c.setEmail("Tche@exemplo.com");
        c.setMatricula("998843");
        c.setNomeCompleto("Tche Bagual");
        c.setNomeUsuario("tchebagual");
        c.setSenha("Senha@123");
        //tenta fazer a inserção
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os dados corretamente, porém o cpf possui letras.
     * deve retornar uma excessão de cpf inválido
     * @throws ValoresIlegaisException 
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCpfletras() throws ValoresIlegaisException {
        System.out.println("CPF com letras");

        Caronista c = new Caronista();
        //insere os dados normalmente
        c.setCampusOrigem("itaqui");
        c.setCpf("17c02a47121");//coloca letras no cpf
        c.setDataNascimento("11/11/1991");
        c.setEmail("roberta@exemplo.com");
        c.setMatricula("98647");
        c.setNomeCompleto("Roberta Bolaños");
        c.setNomeUsuario("robertabol");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Testa a inserção de um cpf com menos de 11 dígitos, que denomina um cpf inválido
     * portanto deve retornar uma excessão de cpf inválido.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCpfMenosDigitos() throws ValoresIlegaisException {
        System.out.println("CPF com menos de 11 digitos");

        Caronista c = new Caronista();
        //insere os dados normalmente
        c.setCampusOrigem("alegrete");
        c.setCpf("170247121");//atribui um cpf com menos de 11 dígitos
        c.setDataNascimento("02/01/1991");
        c.setEmail("joao@exemplo.com");
        c.setMatricula("98647");
        c.setNomeCompleto("João Silva");
        c.setNomeUsuario("joaosilva");
        c.setSenha("Senha@123");
        //tenta iserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os dados corretamente porém o cpf contém mais de 11 dígitos,
     * o que caracteriza cpf inválido. portanto deve retornar excessão
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCpfMaisDigitos() throws ValoresIlegaisException {
        System.out.println("CPF com mais de 11 digitos");

        Caronista c = new Caronista();
        //atribui os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setCpf("517690529923");//insere um cpf com mais de 11 dígitos
        c.setDataNascimento("02/02/1991");
        c.setEmail("marcelo@exemplo.com");
        c.setMatricula("686499");
        c.setNomeCompleto("Marcelo Souza");
        c.setNomeUsuario("masouza");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os valores corretamente, porém a matricula contém letras
     * portanto deve-se retornar uma excessão de Matricula inválida.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testMatriculaLetras() throws ValoresIlegaisException {
        System.out.println("Matricula com letras.");

        Caronista c = new Caronista();
        //insere os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setCpf("86314176964");
        c.setDataNascimento("02/02/1991");
        c.setEmail("felipemoura@exemplo.com");
        c.setMatricula("686as49"); // insere uma matricula com letras
        c.setNomeCompleto("Felipe Moura");
        c.setNomeUsuario("Mfelipe");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere-se todos os dados corretamente porém a matricula é deixada nula
     * deve retornar uma excessão de matricula inválida
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testMatriculaNula() throws ValoresIlegaisException {
        System.out.println("Matricula Nula");

        Caronista c = new Caronista();
        //insere os dados normalmente
        c.setCampusOrigem("alegrete");
        c.setCpf("08424960416");
        c.setDataNascimento("02/02/1991");
        c.setEmail("walter@exemplo.com");
        c.setMatricula("");//esquece-se de por a matricula
        c.setNomeCompleto("Walter Negrão");
        c.setNomeUsuario("walterneg");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os dados corretamente porém com uma data de nascimento inválida.
     * deve retornar uma excessão de Data de Nascimento inválida.
     *
     * Teste para o caso de Uso Inserir Caronista
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testNascimentoInvalido() throws ValoresIlegaisException {
        System.out.println("Data de Nascimento inválida");

        Caronista c = new Caronista();
        //insere os dados normalmente
        c.setCampusOrigem("alegrete");
        c.setCpf("76203611115");
        c.setDataNascimento("02/02/1999");//atribui uma data inválida (menor de 18 anos)
        c.setEmail("chicomuli@exemplo.com");
        c.setMatricula("76346");
        c.setNomeCompleto("Francisco Mulin");
        c.setNomeUsuario("chicomuli");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os dados corretamente porém esquece-se de inserir uma data de nascimento
     * portanto deve retornar uma excessão de data de nascimento nula.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testNascimentoNulo() throws ValoresIlegaisException {
        System.out.println("Data de Nascimento nula");

        Caronista c = new Caronista();
        //atribui os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setCpf("23415179605");
        c.setDataNascimento("");//coloca-se a data de nascimento nula
        c.setEmail("daianeguimaraes@exemplo.com");
        c.setMatricula("76346");
        c.setNomeCompleto("Daiane Guimarães");
        c.setNomeUsuario("daianegui");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os dados corretamente e o campo de nome fica em branco
     * deve retornar uma excessão de nome inválido
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testNomeNulo() throws ValoresIlegaisException {
        System.out.println("Nome nulo");

        Caronista c = new Caronista();
        //insere os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setCpf("90183212495");
        c.setDataNascimento("22/10/1994");
        c.setEmail("Daniel@exemplo.com");
        c.setMatricula("8735");
        c.setNomeCompleto("");//insere nome de usuário nulo
        c.setNomeUsuario("daniel");
        c.setSenha("Senha@123");
        //tenta cadastrar
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Todos os dados são inseridos corretamente porém o Campus inserido
     * não existe. Portanto deve retornar uma excessão de Campus Inválido
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCampusInvalido() throws ValoresIlegaisException {
        System.out.println("Campus de Origem Inválido");

        Caronista c = new Caronista();
        //insere os dados corretamente
        c.setCampusOrigem("Santa Rosa");//insere um campus inválido
        c.setCpf("57343898205");
        c.setDataNascimento("22/10/1992");
        c.setEmail("Willian@exemplo.com");
        c.setMatricula("873345");
        c.setNomeCompleto("Willian José");
        c.setNomeUsuario("willjoe");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Todos os dados são inseridos corretamente porém o Campus
     * não foi inserido . Portanto deve retornar uma excessão de Campus Inválido
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testCampusNulo() throws ValoresIlegaisException {
        System.out.println("Campus de Origem Nulo");

        Caronista c = new Caronista();
        //atribui os dados corretamente
        c.setCampusOrigem("");//não insere o campus
        c.setCpf("75339584119");
        c.setDataNascimento("11/09/1992");
        c.setEmail("nayaraflores@exemplo.com");
        c.setMatricula("897765");
        c.setNomeCompleto("Nayara Flores");
        c.setNomeUsuario("nayflores");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os demais valores corretamente porém também
     * insere um email com fornato inválido, deve retornar a excessão de Email inválido
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testEmailInvalido() throws ValoresIlegaisException {
        System.out.println("Email inválido");

        Caronista c = new Caronista();
        //insere os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setCpf("29727464734");
        c.setDataNascimento("10/08/1982");
        c.setEmail("brunoleroy@exemplo");//insere um email inválido
        c.setMatricula("8948763");
        c.setNomeCompleto("Bruno Leroy");
        c.setNomeUsuario("brunol");
        c.setSenha("Senha@123");
        //tenta iserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os valores corretamente porém não se insere um email
     * deve retornar a excessão de email inválido
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testEmailNulo() throws ValoresIlegaisException {
        System.out.println("Email nulo");

        Caronista c = new Caronista();
        //insere os dados com destreza
        c.setCampusOrigem("alegrete");
        c.setCpf("55868561406");
        c.setDataNascimento("09/02/1972");
        c.setEmail("");//não insere um email
        c.setMatricula("7642");
        c.setNomeCompleto("Giovani Braga");
        c.setNomeUsuario("giovanibr");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os valores corretamente porém no email foi inserido 2 arrobas
     * portanto deve retornar e excessão de valores inválidos
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testEmail2arroba() throws ValoresIlegaisException {
        System.out.println("Email com duas arrobas");

        Caronista c = new Caronista();
        //insere os dados perfeitamente
        c.setCampusOrigem("jaguarao");
        c.setCpf("15441921269");
        c.setDataNascimento("28/07/1991");
        c.setEmail("luciana@@exemplo.com");//coloca 2 @ no email
        c.setMatricula("372864");
        c.setNomeCompleto("Luciana Richards");
        c.setNomeUsuario("lulurich");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os dados corretamente porém no email não foi posto uma arroba
     * deve retornar uma excessão de email inválido.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testEmailSemArroba() throws ValoresIlegaisException {
        System.out.println("Email Sem Arroba");

        Caronista c = new Caronista();
        //insere os dados com esperteza
        c.setCampusOrigem("dom pedrito");
        c.setCpf("11883560705");
        c.setDataNascimento("19/12/1989");
        c.setEmail("augustoexemplo.com");//insere o email sem @
        c.setMatricula("463842");
        c.setNomeCompleto("Augusto Dangui");
        c.setNomeUsuario("gutodangui");
        c.setSenha("Senha@123");
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere todos os valores corretamente porém uma senha "fraca" foi inserida
     * isso quer dizer que ela não corresponde com os parâmetros de força:
     * Caracteres especiais, maiúsculas, minúsculas e números.
     * Deve retornar uma excessão de senha inválida.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testSenhaFraca() throws ValoresIlegaisException {
        System.out.println("Senha fraca");

        Caronista c = new Caronista();
        //insere os dados com maestria
        c.setCampusOrigem("itaqui");
        c.setCpf("435431414700");
        c.setDataNascimento("31/04/1986");
        c.setEmail("yurimarxs@email.com");
        c.setMatricula("63289");
        c.setNomeCompleto("Yuri Marx");
        c.setNomeUsuario("yurimarx");
        c.setSenha("passfraca");//insere uma senha fraca

        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere uma senha com mais de 10 caracteres, portanto deve lançar uma excessão 
     * de Senha inválida. 
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testSenhaCOmMais() throws ValoresIlegaisException {
        System.out.println("Senha Com mais de 10 caracteres");

        Caronista c = new Caronista();
        //insere os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setCpf("57555767775");
        c.setDataNascimento("31/12/1991");
        c.setEmail("ricardofarias@email.com");
        c.setMatricula("7682");
        c.setNomeCompleto("Ricardo Farias");
        c.setNomeUsuario("Rfarias");
        c.setSenha("Senha@MaisDe10");//insere uma senha com mais de 10 caracteres
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere uma senha com menos de 6 caracteres, portanto é uma senha inválida
     * Deve retornar uma excessão de Senha inválida.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testSenhaComMenos() throws ValoresIlegaisException {
        System.out.println("Senha Com menos de 6 caracteres");

        Caronista c = new Caronista();
        //insere os dados como mamae mandou
        c.setCampusOrigem("uruguaiana");
        c.setCpf("26465464594");
        c.setDataNascimento("21/11/1982");
        c.setEmail("rafaelbatista@email.com");
        c.setMatricula("76373");
        c.setNomeCompleto("Rafael Batista");
        c.setNomeUsuario("batistarafael");
        c.setSenha("Men@6");//insere uma senha com menos de 6 caracteres
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere uma senha com apenas dígitos, portanto não obedece os padrões de senha
     * definidos, então deve retornar uma excessão de senha inválida.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testSenhaComDigitos() throws ValoresIlegaisException {
        System.out.println("Senha Com apenas Digitos");

        Caronista c = new Caronista();
        //insere os dados como deveria
        c.setCampusOrigem("alegrete");
        c.setCpf("19258695631");
        c.setDataNascimento("15/05/1959");
        c.setEmail("alexandrevb@email.com");
        c.setMatricula("8787354");
        c.setNomeCompleto("Alexandre Vilas Boas");
        c.setNomeUsuario("alexandrevb");
        c.setSenha("15051959");//coloca apenas digitos na senha
        //tenta iserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere uma senha com apenas Letras, portanto não obedece os padrões de senha
     * definidos, então deve retornar uma excessão de senha inválida.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testSenhaComLetras() throws ValoresIlegaisException {
        System.out.println("Senha Com apenas Letras");

        Caronista c = new Caronista();
        //insere os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setCpf("98361428275");
        c.setDataNascimento("11/05/1979");
        c.setEmail("marinasilva@email.com");
        c.setMatricula("98746");
        c.setNomeCompleto("Marina Silva");
        c.setNomeUsuario("marinasv");
        c.setSenha("marimae");//coloca apenas letras na senha
        //tenta iserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere uma senha com apenas letras minusculas, portanto não obedece os 
     * padrões de senha definidos, então deve retornar uma excessão de senha inválida.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testSenhaComMinusculas() throws ValoresIlegaisException {
        System.out.println("Senha Com apenas Minusculas");

        Caronista c = new Caronista();
        //coloca os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setCpf("14332349474");
        c.setDataNascimento("04/10/1993");
        c.setEmail("amandanardelli@email.com");
        c.setMatricula("8464");
        c.setNomeCompleto("Amanda Nardelli");
        c.setNomeUsuario("amandanll");
        c.setSenha("no@more2");//insere a senha com apenas letras minusculas
        //tenta iserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Insere uma senha com apenas letras maiusculas, portanto não obedece os padrões de senha
     * definidos, então deve retornar uma excessão de senha inválida.
     * @throws ValoresIlegaisException 
     *
     * Teste para o caso de Uso Inserir Caronista
     */
    @Test(expected = ValoresIlegaisException.class)
    public void testSenhaComMaiusculas() throws ValoresIlegaisException {
        System.out.println("Senha Com apenas letras maiusculas.");

        Caronista c = new Caronista();
        //insere os dados corretamente
        c.setCampusOrigem("bage");
        c.setCpf("47952817850");
        c.setDataNascimento("12/12/1992");
        c.setEmail("andressaalves@email.com");
        c.setMatricula("9636");
        c.setNomeCompleto("Andressa Alves");
        c.setNomeUsuario("andressaalves");
        c.setSenha("DESSA@22");//coloca a senha com apenas letras maiusculas
        //tenta inserir
        AuxCaronista instance = new AuxCaronista();
        instance.createCaronista(c);
    }

    /**
     * Modifica o Caronista. busca um caronista pelo cpf e modifica ele de acordo
     * com os dados permitidos. Caso esteja correto, o caronista obtido no início
     * deve diferir do caronista enviado no fim do algoritmo de teste
     * 
     * @throws ValoresIlegaisException 
     * 
     * Teste para o Caso de Uso Modifica Caronista
     */
    @Test
    public void testModificaCaronista() throws ValoresIlegaisException {

        System.out.println("Testa método de modificar caronista");
        AuxCaronista ax = new AuxCaronista();
        //retorna um caronista
        Caronista c = ax.retornaCaronista("83953666630");
        //atribui os novos dados corretamente
        c.setCampusOrigem("alegrete");
        c.setDataNascimento("12/10/1990");
        c.setEmail("cadu@exemplo.com");
        c.setMatricula("2281");
        c.setNomeCompleto("Carlos Eduardo Coelho");
        c.setNomeUsuario("carlosdudu");
        c.setSenha("123@Senha");
        //chama método de modificar
        ax.editCaronista(c);
        //retorna o caronista modificado
        Caronista cModificado = ax.retornaCaronista("83953666630");
        //compara os dois
        assertTrue(c != cModificado);
    }

    /**
     * Para modificar, é necessário uma busca. Neste teste é feita a busca
     * mas o registro não é encontrado.
     * 
     * @throws ValoresIlegaisException 
     * Teste para o Caso de Uso Modifica Caronista
     */
    @Test
    public void registroInexistente() throws ValoresIlegaisException {
        AuxCaronista ax = new AuxCaronista();
        //busca por um caronista não existente
        Caronista c = ax.retornaCaronista("47952817850");

        assertTrue(c == null);


    }

    /**
     * Faz uma busca de caronista, só que não insere parêmetros, portanto deve retornar
     * erro.
     * 
     * Teste para o caso de Uso Modifica Caronista
     * @throws ValoresIlegaisException
     * @throws SQLException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void buscaNula() throws ValoresIlegaisException, SQLException {
        AuxCaronista ax = new AuxCaronista();
        //busca caronista sem parâmetros.
        ArrayList<Caronista> listaCaronista = ax.buscaCaronistaCampus("");

        assertTrue(listaCaronista.isEmpty());

    }

    /**
     * Tenta Modificar o caronista porém coloca-se uma matricula nula.
     * Deve lançar um erro.
     *
     * Teste para o caso de uso Modifica Caronista
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void modificaMatriculaNula() throws ValoresIlegaisException {
        AuxCaronista ax = new AuxCaronista();
        //busca o caronista no Banco de Dados
        Caronista c = ax.retornaCaronista("83953666630");
        //insere os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setDataNascimento("12/10/1990");
        c.setEmail("cadu@exemplo.com");
        c.setMatricula("");//coloca uma matricula nula
        c.setNomeCompleto("Edna Santos");
        c.setNomeUsuario("ednast");
        c.setSenha("123@Senha");
        //tenta editar
        ax.editCaronista(c);

    }

    /**
     * Tenta modificar a data de nascimento, para uma data inválida.
     * deve retornar um erro.
     * 
     * Teste para o caso de uso Modifica Caronista
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void modificaNascimentoInvalido() throws ValoresIlegaisException {
        AuxCaronista ax = new AuxCaronista();
        //busca caronista pela matricula
        Caronista c = ax.retornaCaronista("83953666630");
        //atribui os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setDataNascimento("12/10/1996");//coloca uma data inválida
        c.setEmail("cadu@exemplo.com");
        c.setMatricula("8846");
        c.setNomeCompleto("Carlos Coelho");
        c.setNomeUsuario("cllho");
        c.setSenha("123@Senha");
        //tenta editar
        ax.editCaronista(c);

    }

    /**
     * Tenta modificar a data de nascimento do caronista, porém é posto um valor
     * nulo, deve retornar um erro.
     * 
     * Teste para o caso de uso Modifica Caronista
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void modificaNascimentoNulo() throws ValoresIlegaisException {
        AuxCaronista ax = new AuxCaronista();
        //retorna um caronista pelo cpf
        Caronista c = ax.retornaCaronista("83953666630");
        //edita os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setDataNascimento("");//coloca a data de nascimento nula
        c.setEmail("cadu@exemplo.com");
        c.setMatricula("8846");
        c.setNomeCompleto("Carlos Coelho");
        c.setNomeUsuario("cllho");
        c.setSenha("123@Senha");
        c.setCpf("09330278930");
        //tenta editar
        ax.editCaronista(c);

    }

    /**
     * Tenta fazer a modificação de caronista, porém é inserido um nome nulo
     * Deve retornar um erro.
     * 
     * Teste para o caso de uso Modifica Caronista
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void modificaNomeNulo() throws ValoresIlegaisException {
        AuxCaronista ax = new AuxCaronista();
        //retorna caronista pelo cpf
        Caronista c = ax.retornaCaronista("83953666630");
        //edita os dados corretamente
        c.setCampusOrigem("alegrete");
        c.setDataNascimento("12/10/1992");
        c.setEmail("cadu@exemplo.com");
        c.setMatricula("8846");
        c.setNomeCompleto("");//coloca o nome nulo
        c.setNomeUsuario("cllho");
        c.setSenha("123@Senha");
        c.setCpf("83953666630");
        //tenta editar
        ax.editCaronista(c);

    }

    /**
     * Tenta modificar o caronista, porém insere um campus inválido.
     * Deve retornar um erro
     * 
     * Teste para o caso de uso Modifica Caronista
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void modificaCampusInvalido() throws ValoresIlegaisException {
        AuxCaronista ax = new AuxCaronista();
        //retorna caronista pelo cpf
        Caronista c = ax.retornaCaronista("83953666630");
        //edita os dados corretamente
        c.setCampusOrigem("Florianopolis");//coloca um campus inválido
        c.setDataNascimento("12/10/1991");
        c.setEmail("cadu@exemplo.com");
        c.setMatricula("8846");
        c.setNomeCompleto("Carlos Coelho");
        c.setNomeUsuario("cllho");
        c.setSenha("123@Senha");
        //tenta editar
        ax.editCaronista(c);

    }

    /**
     * Tenta modificar o caronista, porém não insere nenhum campus.
     * Deve retornar um erro
     * 
     * Teste para o caso de uso Modifica Caronista
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void modificaCampusNulo() throws ValoresIlegaisException {
        AuxCaronista ax = new AuxCaronista();
        //retorna um caronista pelo cpf
        Caronista c = ax.retornaCaronista("83953666630");
        //edita os campos corretamente
        c.setCampusOrigem("");//coloca o campus de origem inválido
        c.setDataNascimento("12/10/1996");
        c.setEmail("cadu@exemplo.com");
        c.setMatricula("8846");
        c.setNomeCompleto("Carlos Coelho");
        c.setNomeUsuario("cllho");
        c.setSenha("123@Senha");
        //tenta editar
        ax.editCaronista(c);

    }

    /**
     * Insere todos os dados corretamente, e cancela a carona
     * 
     * Teste para o Caso de uso Cancelar Carona
     * @throws ValoresIlegaisException 
     */
    @Test
    public void cancelaCaronaSucesso() throws ValoresIlegaisException {
        AuxCarona axca = new AuxCarona();
        Carona ca = new Carona();
        AuxCaronista ax = new AuxCaronista();
        //insere os dados corretamente 
        ca.setCampusDestino("alegrete");
        ca.setCampusOrigem("sao borja");
        ca.setCaronista(ax.retornaCaronista("83953666630"));
        ca.setDataHora_Chegada("11/12/2012");
        ca.setDataHora_Saida("10/11/2012");
        ca.setDataHora_registro("8/12/2012");
        ca.setMaterial("");
        //insere a carona no BD
        axca.insereCarona(ca);
        //se a carona for cancelada retorna true
        assertTrue(axca.cancelaCarona(ca));
    }

    /**
     * Insere todos os dados corretamente,porém não adiciona
     * tenta depois inserir a carona
     * 
     * Teste para o Caso de uso Cancelar Carona
     * @throws ValoresIlegaisException 
     */
    @Test
    public void cancelaCaronaInexistente() throws ValoresIlegaisException {
        AuxCarona axca = new AuxCarona();
        Carona ca = new Carona();
        AuxCaronista ax = new AuxCaronista();
        //insere os dados corretamente
        ca.setCampusDestino("alegrete");
        ca.setCampusOrigem("sao borja");
        ca.setCaronista(ax.retornaCaronista("83953666630"));
        ca.setDataHora_Chegada("11/12/2012");
        ca.setDataHora_Saida("10/11/2012");
        ca.setDataHora_registro("8/12/2012");
        ca.setMaterial("");
        //tenta cancelar a carona sem tê-la adicionado.
        //deve retornar falso
        assertFalse(axca.cancelaCarona(ca));


    }

    /**
     * Busca a carona no Banco de dados, porém sem parâmetros
     * Deve retornar uma excessão
     * 
     * Teste para o Caso de uso Cancelar Carona
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void buscaCaronacamposNulos() throws ValoresIlegaisException {
        AuxCarona acxa = new AuxCarona();
        //busca a carona sem parâmetros
        //deve lançar excessão
        acxa.retornaCaronaPCaronista("");

    }

    /**
     * Insere todos os dados corretamente, porém menos de 24:00 pra cancelar a carona
     * Deve retornar uma excessão
     * 
     * Teste para o Caso de uso Cancelar Carona
     * @throws ValoresIlegaisException 
     */
    @Test(expected = ValoresIlegaisException.class)
    public void janeladeTempoUltrapassada() throws ValoresIlegaisException {
        Carona ca = new Carona();
        AuxCaronista axct = new AuxCaronista();
        AuxCarona axca = new AuxCarona();
        //insere todos os dados corretamente
        ca.setCampusDestino("itaqui");
        ca.setCampusOrigem("sao borja");
        ca.setCaronista(axct.retornaCaronista("83953666630"));
        ca.setDataHora_Chegada("20/03/2012");
        ca.setDataHora_Saida("19/10/2012");
        ca.setDataHora_registro("19/03/2012");
        ca.setMaterial("material");
        //insere a carona no BD
        axca.insereCarona(ca);
        //tenta cancelar a carona em seguida
        axca.cancelaCarona(ca);

    }
}
