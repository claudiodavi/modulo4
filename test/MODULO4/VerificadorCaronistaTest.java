/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MODULO4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author claudio
 */
public class VerificadorCaronistaTest {

    public VerificadorCaronistaTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Usa uma senha com somente letras maiúsculas,
     * deve retornar falso pois não possui letras minusculas que fazem parte
     * do padrão de "Senha forte"
     */
    @Test
    public void testVerificaSenhaForteMaius() {

        String senha = "SENHA@123";

        System.out.println("verificaSenhaForte Senha só maiúsculas");
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaSenhaForte(senha);
        assertEquals(expResult, result);

    }

    /**
     * Usa uma senha com somente letras minusculas,
     * deve retornar falso pois não possui letras maiusculas que fazem parte
     * do padrão de "Senha forte"
     */
    @Test
    public void testVerificaSenhaForteMin() {

        String senha = "senha@123";

        System.out.println("verificaSenhaForte letras minusculas");
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaSenhaForte(senha);
        assertEquals(expResult, result);

    }

    /**
     * Usa uma senhasem caracteres especiais,
     * deve retornar falso pois não possui caracteres especiais que fazem parte
     * do padrão de "Senha forte"
     */
    @Test
    public void testVerificaSenhaEspc() {

        String senha = "Senha123";

        System.out.println("Verifica senha forte sem caracteres especiais");

        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaSenhaForte(senha);
        assertEquals(expResult, result);

    }

    /**
     * Usa uma senha sem números,
     * deve retornar falso pois não possui numeros que fazem parte
     * do padrão de "Senha forte"
     */
    @Test
    public void testVerificaSenhaNum() {

        String senha = "Senha@";

        System.out.println("Verifica senha forte sem numeros");

        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaSenhaForte(senha);
        assertEquals(expResult, result);

    }

    /**
     * Senha nos padrões de senha forte, o teste deve retornar positivo
     * há letras maiúsculas, minúsculas, numeros e caracteres especiais.
     */
    @Test
    public void testVerificaSenhaFORTE() {

        String senha = "Senha@123";

        System.out.println("Verifica senha forte");

        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = true;
        boolean result = instance.verificaSenhaForte(senha);
        assertEquals(expResult, result);

    }

    /**
     * Verifica se o email é válido, o resultado deve ser positivo, pois atende
     * os parâmetros de validação de email.
     */
    @Test
    public void testVerificaEmailValidoCerto() {

        String email = "exemplo@exemplo.com";

        System.out.println("verificaEmailValido CERTO.");

        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = true;
        boolean result = instance.verificaEmailValido(email);
        assertEquals(expResult, result);
    }

    /**
     * Verifica email válido sem arroba, deve retornar falso.
     */
    @Test
    public void testVerificaEmailValidoSemArro() {

        String email = "exemploexemplo.com";

        System.out.println("verificaEmailValido sem arroba.");

        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaEmailValido(email);
        assertEquals(expResult, result);
    }

    /**
     * verifica se o email é validado sem o ".". deve retornar falso.
     */
    @Test
    public void testVerificaEmailValidoSemPonto() {

        String email = "exemplo@exemplo";

        System.out.println("verificaEmailValido sem '.'");

        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaEmailValido(email);
        assertEquals(expResult, result);
    }

    /**
     * Verifica CPF com numeros iguais. Porém com 11 dígitos.
     */
    @Test
    public void testVerificaCPFNumIgual() {

        String cpf = "00000000000";

        System.out.println("verificaCPF com numeros iguais.");
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaCPFvalido(cpf);
        assertEquals(expResult, result);
    }

    /**
     * Verifica CPF com numeros inválido. Porém com 11 dígitos.
     */
    @Test
    public void testVerificaCPFNumAleat() {

        String cpf = "87645683407";

        System.out.println("verificaCPF com números aleatórios");
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaCPFvalido(cpf);
        assertEquals(expResult, result);
    }

    /**
     * Verifica CPF com numeros iguais. Porém com 11 dígitos.
     */
    @Test
    public void testVerificaCPFMenosDigitos() {

        String cpf = "04094828";

        System.out.println("verificaCPF com menos de 11 dígitos");
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaCPFvalido(cpf);
        assertEquals(expResult, result);
    }

    /**
     * CPF correto 11 dígitos e válido. deve retornar true.
     */
    @Test
    public void testVerificaCPFCerto() {

        String cpf = "09330278930";

        System.out.println("verificaCPF correto");
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = true;
        boolean result = instance.verificaCPFvalido(cpf);
        assertEquals(expResult, result);
    }

    /**
     * Teste que verifica a matricula. deve retornar true, pois só possui numeros
     */
    @Test
    public void testVerificaMatriculaCerto() {


        System.out.println("verificaMatricula correto.");

        String matricula = "123453";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = true;
        boolean result = instance.verificaMatricula(matricula);
        assertEquals(expResult, result);
    }

    /**
     * Teste que verifica se a matricula está correta, no caso, 
     * não deve haver letras
     */
    @Test
    public void testVerificaMatriculaComLetra() {
        System.out.println("verificaMatricula com letras");

        String matricula = "matricula";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaMatricula(matricula);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail
    }

    /**
     * Teste que verifica se a matricula está correta, no caso, 
     * não deve haver letras
     */
    @Test
    public void testVerificaMatriculaComLetraENum() {
        System.out.println("verificaMatricula com letras e numeros");

        String matricula = "123matricula123";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaMatricula(matricula);
        assertEquals(expResult, result);
    }

    /**
     * Teste que verifica se o campus de origem é existente e válido.
     */
    @Test
    public void testVerificaCampusOrigemCerto() {

        System.out.println("verificaCampusOrigem existente");

        String campusorigem = "alegrete";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = true;
        boolean result = instance.verificaCampusOrigem(campusorigem);
        assertEquals(expResult, result);
    }

    /**
     * Teste que verifica se o campus de origem é existente e válido.
     * no caso, deve retornar falso, pois o campus é inexistente
     */
    @Test
    public void testVerificaCampusOrigemErrado() {

        System.out.println("verificaCampusOrigem campus inexistente");

        String campusorigem = "Porto Alegre";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaCampusOrigem(campusorigem);
        assertEquals(expResult, result);
    }

    /**
     * Teste que recebe uma data válida, converte pra Date e testa se o usuário
     * é maior de 18 anos.
     */
    @Test
    
    public void testVerificaDataNascimentoCERTO() throws ParseException {

        System.out.println("verificaDataNascimento CERTO");

        String datanasc = "04/08/1982";
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date data = (Date) formatter.parse(datanasc);

        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = true;
        boolean result = instance.verificaDataNascimento(data);
        assertEquals(expResult, result);

    }

    /*
     * Testa se o método verifica se o usuário é menor de idade. Deve retornar
     * falso.
     */
    @Test
    public void testVerificaDataNascimentoMenorIdade() throws ParseException {
        System.out.println("verificaDataNascimento menor de idade");

        String datanasc = "04/08/1998";
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date data = (Date) formatter.parse(datanasc);

        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaDataNascimento(data);
        assertEquals(expResult, result);
    }

    /**
     * Testa se o nome de usuário já existe no Banco de Dados.
     */
    public void testVerificaNomeUsuario() {
        System.out.println("verificaNomeExistente já existe");


        String nome = "Davi";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaNomeUsuario(nome);
        assertEquals(expResult, result);
    }

    /**
     * Testa se o nome de usuário não existe no BD
     */
    public void testVerificaNomeUsuarioInex() {
        System.out.println("verificaNomeExistente não existe");

        String nome = "Claudio";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = true;
        boolean result = instance.verificaNomeUsuario(nome);
        assertEquals(expResult, result);
    }

    /**
     * Teste que verifica se o método de verificar cpf existente está correto.
     * deve retornar true pois o cpf não está incluido.
     */
    public void testVerificaCPFExistente() {
        System.out.println("verificaCPFExistente que não foi incluido");
        String cpf = "45234636542";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = true;
        boolean result = instance.verificaCPFExistente(cpf);
        assertEquals(expResult, result);
    }

    /**
     * Teste que verifica se o método de verificar cpf existente está correto.
     * deve retornar true pois o cpf não está incluido.
     */
    public void testVerificaCPFExistenteNops() {
        System.out.println("verificaCPFExistente que já foi incluido ");
        String cpf = "09330278930";
        VerificadorCaronista instance = new VerificadorCaronista();
        boolean expResult = false;
        boolean result = instance.verificaCPFExistente(cpf);
        assertEquals(expResult, result);
    }

}
